<?php

namespace App\Notification;

use App\Entity\ContactForm;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class ContactNotification{

    public function __construct(\Swift_Mailer $mailer, Environment $renderer)
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;}

    public function notify(ContactForm $contact)
    {
            $message = (new \Swift_Message('Informations'))
                ->setFrom($contact->getEmail())
                ->setTo('contact@tsioryras.eu')
                ->setReplyTo($contact->getEmail())
                ->setBody($this->renderer->render('default/message.html.twig', [
                    'contact' => $contact,
                    'subject' => 'INFORMATIONS'
                ]), 'text/html');
        $this->mailer->send($message);
            return ($this->mailer->send($message));
    }
}