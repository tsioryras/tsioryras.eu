<?php

namespace App\Controller;

use App\Entity\ContactForm;
use App\Form\ContactFormType;
use App\Notification\ContactNotification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

        /**
         * @Route("/", name="default")
         * @param Request $request
         * @param ContactNotification $notification
         * @return \Symfony\Component\HttpFoundation\Response
         */
        function index(Request $request, ContactNotification $notification)
        {
            $contact = new ContactForm();
            $form = $this->createForm(ContactFormType::class, $contact);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $sending_state = $notification->notify($contact);
                if ($sending_state) {
                    $response = new JsonResponse(array(
                        'message'=> 'Message envoyé',
                        'status'=>200,
                    ), 200);
                }else{
                    $response = new JsonResponse(array(
                        'message'=> 'Erreur! réessayer',
                        'status'=>403,
                    ), 403);
                }
                return $response;
            }
            return $this->render('default/index.html.twig', [
                'formContact' => $form->createView()
            ]);
        }
}
